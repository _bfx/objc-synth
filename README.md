# Coding challenge

*My remarks in Italic. [Benjamin]*

## Introduction

This project contains a simple audio synthesizer written in Swift.
**Try it out and make yourself comfortable with the code!**

The heart of this project is in the `Synth.swift` file which contains the code
responsible for creating the sound (with help from `Oscillator.swift`)


## Todo

- Rewrite `Synth.swift` and `Oscillator.swift` in **Objective-C** and add it to this project
- Remove those two Swift files from the project and test it with your Obj-C implementation 
- They must work the same way as their counterpart in Swift
- Describe (not implement) how you would add the possibility to have 3 voices (synthesizers) at the same time
    - *The Synth class would have to provide two more AVAudioSourceNodes. The client, which currently is the view controller, would reference individual nodes (voices) by a fixed index for oscillator configuration and parameter changes. Internally, the additional nodes would also have to be attached and connected to the audio engine's main mixer. Individual voice volumes would have to be set on the mixer node's input busses. The user interface could continue to support single touch control to play fixed chord structures (e.g. major, minor) and also offer the possibility to use multi touch control to play several different tones at the same time.*

## Bonus

- Write a method in the `Synth.m` resp. `Synth.h` that informs the UI whenever audio is playing
    - *I hope this does not come across nitpicky, but I was not entirely sure about the definition of "audio is playing". Technically, audio data is streaming constantly once the audio engine started, it just may happen to be all zeroes. I chose the definition "sound should be audible", i.e. there is a non-zero audio signal and the audio engine's volume is turned up so we can assume the signal has an amplitude greater than zero.*
- Display in the UI the String "Playing..." in some way when this method returns true, "" when it's false


## Rules

1. Create a repo on Github or Bitbucket and push this project (Don't fork!)
2. Make commits as often as possible! *(We want see what you do and how you do it)*
3. When you finish, send us the link to your repo by email
4. Please don't spend more than two hours on it! (1h should be fine)


### Why we use coding challenges to interview developers

Hiring is one of the most time-intensive and critical processes.
We know there are numbers of different approaches on this (and some pretty passionate opinions about it) but this approach works for us.
We first start with a coding challenge (like this one) in order to quickly evaluate the coding level of the candidate. Depending on the results of this challenge, we will invite you for more thorough interviews such as cultural fit and more technical knowledge. Recruiting is a mutual process. Feel free to ask us any questions as well.
Good luck!

What we want to evaluate with this coding challenge::

**Can you write any code?**
Basically, we need to see if you can write any code at all. We believe that you can. Still, there are many wantacoders out there and it's difficult to tell from a CV only.
Moreover, you’d be surprised how many candidates fall short when it’s time to put the cursor to the editor.

This is a pre-screening and we keep it really short. 1-2h should be fine for a skilled programmer to complete the task.
That's the only code you have to write. No online-challenges, coding puzzles or similar...
If we are happy with the result, we would like to invite you for further interviews on a personal level.

We know at some companies, it can be frustrating to send in coding challenges and never hear back from them anymore.
That's **not** how **we** work and think! So, you will get a feedback from us, no matter if we want to continue with you or not.

PROMISED!

Again, Good Luck!
The Pictural team
