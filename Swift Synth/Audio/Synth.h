#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Oscillator.h"

NS_ASSUME_NONNULL_BEGIN

@class Synth;
@protocol SynthDelegate <NSObject>

- (void)synth:(Synth *)synth isPlaying:(BOOL)isPlaying;

@end

@interface Synth : NSObject

@property (nonatomic, weak) id<SynthDelegate> delegate;
@property (class, nonatomic, strong, readonly) Synth *shared;
@property (nonatomic, assign) Float32 volume;

- (void)setWaveformTo:(Signal)signal;

@end

NS_ASSUME_NONNULL_END
