#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef float (^Signal)(float);

typedef NS_ENUM(NSInteger, Waveform) {
    sine,
    triangle,
    sawtooth,
    square,
    whiteNoise,
};

@interface Oscillator : NSObject

@property (class, nonatomic, assign) float amplitude;
@property (class, nonatomic, assign) float frequency;

@property (class, nonatomic, readonly) Signal sine;
@property (class, nonatomic, readonly) Signal triangle;
@property (class, nonatomic, readonly) Signal sawtooth;
@property (class, nonatomic, readonly) Signal square;
@property (class, nonatomic, readonly) Signal whiteNoise;

@end

NS_ASSUME_NONNULL_END
