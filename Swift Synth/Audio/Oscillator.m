#import "Oscillator.h"

@implementation Oscillator

static float _amplitude = 0.0;
static float _frequency = 0.0;

+ (void)setAmplitude:(float)amplitude
{
    _amplitude = amplitude;
}

+ (float)amplitude
{
    return _amplitude;
}

+ (void)setFrequency:(float)frequency
{
    _frequency = frequency;
}

+ (float)frequency
{
    return _frequency;
}

+ (Signal)sine
{
    return ^float (float time) {
        return _amplitude * sin(2.0 * M_PI * _frequency * time);
    };
}

+ (Signal)triangle
{
    return ^float (float time) {
        double period = 1.0 / (double)_frequency;
        double currentTime = fmod(time, period);
        double value = currentTime / period;
        
        double result = 0.0;
        
        if (value < 0.25)
        {
            result = value * 4;
        }
        else if (value < 0.75)
        {
            result = 2.0 - (value * 4.0);
        }
        else
        {
            result = value * 4 - 4.0;
        }
        
        return result * _amplitude;
    };
}

+ (Signal)sawtooth
{
    return ^float (float time) {
        double period = 1.0 / _frequency;
        double currentTime = fmod(time, period);
        
        return _amplitude * (currentTime / period * 2 - 1.0);
    };
}

+ (Signal)square
{
    return ^float (float time) {
        double period = 1.0 / (double)_frequency;
        double currentTime = fmod(time, period);
        
        return ((currentTime / period) < 0.5) ? _amplitude : -1.0 * _amplitude;
    };
}

+ (Signal)whiteNoise
{
    return ^float (float time) {
        return _amplitude * [Oscillator randomNumberBetween:-1.0 maxNumber:1.0];
    };
}

// https://stackoverflow.com/a/33215424/4408170
+ (float)randomNumberBetween:(float)min maxNumber:(float)max
{
    return min + arc4random_uniform(max - min + 1);
}

@end
