#import "Synth.h"

NS_ASSUME_NONNULL_BEGIN

@interface Synth ()

@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic, assign) float time;
@property (nonatomic, assign) double sampleRate;
@property (nonatomic, assign) float deltaTime;
@property (nonatomic, strong) Signal signal;

@end

NS_ASSUME_NONNULL_END

@implementation Synth

+ (Synth *)shared
{
    static Synth *synth = nil;
    if (synth == nil)
    {
        synth = [[Synth alloc] initWithSignal:Oscillator.sine];
    }
    return synth;
}

- (void)setVolume:(float)volume
{
    self.audioEngine.mainMixerNode.outputVolume = volume;
    
    // Notify delegate if one is available
    if ([self.delegate conformsToProtocol:@protocol(SynthDelegate)])
    {
        BOOL isPlaying = (volume > 0.0) && (self.signal != nil) && self.audioEngine.running;
        [self.delegate synth:self isPlaying:isPlaying];
    }
}

- (float)volume
{
    return self.audioEngine.mainMixerNode.outputVolume;
}

// MARK: Lifecycle

- (instancetype)initWithSignal:(Signal)signal
{
    self = [super init];
    if (self)
    {
        AVAudioEngine *audioEngine = AVAudioEngine.new;
        AVAudioMixerNode *mainMixer = audioEngine.mainMixerNode;
        AVAudioFormat *format = [audioEngine.outputNode inputFormatForBus:0];
        
        if (format == nil || format.sampleRate == 0.0)
        {
            return nil;
        }
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat sampleRate:format.sampleRate channels:1 interleaved:format.isInterleaved];
        
        self.signal = signal;
        AVAudioSourceNode *sourceNode = [self sourceNode];
        [audioEngine attachNode:sourceNode];
        [audioEngine connect:sourceNode to:mainMixer format:inputFormat];
        [audioEngine connect:mainMixer to:audioEngine.outputNode format:nil];
        mainMixer.outputVolume = 0.0;
        
        NSError *error = nil;
        BOOL success = [audioEngine startAndReturnError:&error];
        if (!success || error)
        {
            NSLog(@"Could not start audioEngine: %@", error.localizedDescription);
            return nil;
        }
        
        self.sampleRate = format.sampleRate;
        self.deltaTime = 1.0 / format.sampleRate;
        self.audioEngine = audioEngine;
    }
    
    return self;
}

- (AVAudioSourceNode *)sourceNode
{
    return [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL * _Nonnull isSilence, const AudioTimeStamp * _Nonnull timestamp, AVAudioFrameCount frameCount, AudioBufferList * _Nonnull outputData) {
        
        // Generate the waveform data for this time slice and copy it to the buffer list
        for (AVAudioFrameCount i=0; i<frameCount; i++)
        {
            float sampleVal = self.signal(self.time);
            self.time += self.deltaTime;
            
            for (UInt32 buffer=0; buffer < outputData->mNumberBuffers; buffer++)
            {
                ((float *)outputData->mBuffers[buffer].mData)[i] = sampleVal;
            }
        }
                 
        return noErr;
    }];
}

- (void)setWaveformTo:(Signal)signal
{
    self.signal = signal;
}

@end
